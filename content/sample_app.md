## Sample App

----------

### Main Objectives
- Build a web application on Ruby on Rails
- Create an integration workflow
- Automate deployments  

---

### Complementary
- Project landing page
- Docker environment for build and development

Note:
- Add some interesting context such as "we're getting hired to do ABC"

----------

### App and CI Tour

- Review files and repository
- View CI badge
- Talk about the pipeline and builds
- Talk about environments
- Cycle Analytics
- Activating builds
